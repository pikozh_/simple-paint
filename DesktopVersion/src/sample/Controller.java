package sample;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Controller {

    @FXML
    private Canvas canvas;

    @FXML
    private Label label;
    @FXML
    private CheckBox erase;

    @FXML
    private Slider slider;

    @FXML
    private ColorPicker colorPicker;

    GraphicsContext g;
    public void initialize() {

         g= canvas.getGraphicsContext2D();

        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                label.setText(String.format("%.0f", newValue));
                g.setLineWidth(slider.getValue());
            }
        });

        colorPicker.setOnAction(event -> {
            g.setStroke(colorPicker.getValue());
        });

        canvas.setOnMousePressed(event -> {
            if (erase.isSelected()) {
                g.clearRect(event.getX(), event.getY(), slider.getValue(), slider.getValue());
            } else {
                g.beginPath();
                g.lineTo(event.getX(), event.getY());
                g.stroke();
            }
        });

        canvas.setOnMouseDragged(event -> {

            if (erase.isSelected()) {
                g.clearRect(event.getX(), event.getY(),
                        slider.getValue(), slider.getValue());
            } else {
                g.lineTo(event.getX(), event.getY());
                g.stroke();
            }

            });
    }

    public void onSave() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png"),
                new FileChooser.ExtensionFilter("BMP files (*.bmp)", "*.bmp"),
                new FileChooser.ExtensionFilter("JPEG files (*.jpeg)", "*.jpeg"),
                new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.gif"));

        fileChooser.setInitialFileName("myfile");
        fileChooser.setInitialDirectory(new File("D:\\Documents\\DevEducation\\Simple Paint\\DesktopVersion\\res"));
        File file = fileChooser.showSaveDialog(null);
        if (file != null) {
            try {
                Image image = canvas.snapshot(null, null);
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            } catch (IOException ex) {
                System.out.println("Failed to save image!");
            }
        }
        g.clearRect(0,0,canvas.getWidth(),canvas.getHeight());
    }

    @FXML
    public void onOpen() {
        FileChooser openFile = new FileChooser();
        openFile.setTitle("Open File");
        File file = openFile.showOpenDialog(null);
        if (file != null) {
            try {
                InputStream io = new FileInputStream(file);
                Image img = new Image(io);
                g.drawImage(img, 0, 0);
            } catch (IOException ex) {
                System.out.println("Error!");
            }
        }
    }

    public void clear(){
        g.clearRect(0,0,canvas.getWidth(),canvas.getHeight());
    }

    public void onExit() {
        Platform.exit();
    }

}
